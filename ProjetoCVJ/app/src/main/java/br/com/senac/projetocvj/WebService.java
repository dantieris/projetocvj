package br.com.senac.projetocvj;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.ExecutionException;

public class WebService {
    private static final String URL = "http://projetocvj.esy.es/WS/";

    private static final String SEPARADOR = "/";

    private static final String COTACAO = "COTACAO";

    public static final String DOLAR = "DOLAR";
    public static final String EURO = "EURO";
    public static final String LIBRA = "LIBRA";

    public static double getCotacao(String moeda) {
        double cotacao = 0;

        JSONObject jsonObjectCotacao = null;

        try {
            jsonObjectCotacao = new WebServiceGET().execute(URL + COTACAO + SEPARADOR + moeda).get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }

        if (jsonObjectCotacao != null) {
            try {
                cotacao = jsonObjectCotacao.getDouble(COTACAO);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return cotacao;
    }
}
