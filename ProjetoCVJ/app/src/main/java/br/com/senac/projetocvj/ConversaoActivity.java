package br.com.senac.projetocvj;

import android.app.Activity;
import android.os.Bundle;
import android.text.Editable;
import android.text.format.Formatter;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DecimalFormat;


public class ConversaoActivity extends Activity {

    private Spinner de;
    private Spinner para;

    private double [] cotacoes = {1, 3.5, 3.8, 5.4};
    private String [] nomePluralMoedas = {"Reais", "Dólares", "Euros", "Libras"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_conversao);

        String [] moedas = getResources().getStringArray(R.array.spinner_moedas);

        if ((cotacoes[1] = WebService.getCotacao(WebService.DOLAR)) > 0) {
            cotacoes[1] = 3.5;
        }
        moedas[1] = moedas[1] + " (" + cotacoes[1] + ")";

        if ((cotacoes[2] = WebService.getCotacao(WebService.EURO)) > 0) {
            cotacoes[2] = 3.8;
        }
        moedas[2] = moedas[2] + " (" + cotacoes[2] + ")";

        if ((cotacoes[3] = WebService.getCotacao(WebService.LIBRA)) > 0) {
            cotacoes[3] = 5.4;
        }
        moedas[3] = moedas[3] + " (" + cotacoes[3] + ")";

        Log.i("COTACAO", "Dolar: " + WebService.getCotacao(WebService.DOLAR));
        Log.i("COTACAO", "Euro: " + WebService.getCotacao(WebService.EURO));
        Log.i("COTACAO", "Libra: " + WebService.getCotacao(WebService.LIBRA));

        ArrayAdapter<CharSequence> adapter = new ArrayAdapter<CharSequence>(this, R.layout.spinner_layout_item, moedas);

        de = (Spinner) findViewById(R.id.sp_conversao_de);
        para = (Spinner) findViewById(R.id.sp_conversao_para);

        de.setAdapter(adapter);
        para.setAdapter(adapter);

        de.setSelection(0);
        para.setSelection(1);
    }

    public void converter(View view){
        int itemDe = de.getSelectedItemPosition();
        int itemPara = para.getSelectedItemPosition();

        EditText et_valor = (EditText) findViewById(R.id.et_conversao_valor);

        String texto = String.valueOf(et_valor.getText());

        double valor = -1;

        if (!texto.equalsIgnoreCase("")) {
            valor = Double.parseDouble(texto);
        }

        double resultado = 0;

        if (itemDe == 0 || itemPara == 0) {
            if (valor != -1) {
                if (cotacoes[itemDe] > cotacoes[itemPara]) {
                    resultado = valor * cotacoes[itemDe]; // Realiza a conversão e formata em duas casas decimais.
                } else if (cotacoes[itemDe] < cotacoes[itemPara]) {
                    resultado = valor / cotacoes[itemPara]; // Realiza a conversão e formata em duas casas decimais.
                } else {
                    resultado = valor;
                }

                TextView tv_resultado = (TextView) findViewById(R.id.tv_conversao_resultado);

                DecimalFormat df = new DecimalFormat("0.00");

                tv_resultado.setText("Resultado da conversão " + df.format(resultado) + " " + nomePluralMoedas[itemPara]);
            } else {
                Toast.makeText(this, "Erro: valor de conversão inválido.", Toast.LENGTH_LONG).show();
            }
        } else {
            Toast.makeText(this, "Erro: Não sei fazer esta conversão!", Toast.LENGTH_LONG).show();
        }
    }
}
